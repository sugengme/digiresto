<?php

use Faker\Factory as Faker;
use Petruk\Properties\Property;

class PropertyTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		Property::create([
            'name' => 'Gadang Ambo Sederhana',
            'address' => $faker->address,
            'phone1' => $faker->phoneNumber,
            'phone2' => $faker->phoneNumber,
            'email'  => $faker->email,
            'website' => $faker->url
        ]);
	}

}