<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('category_id');
            $table->string('name');
            $table->decimal('price', 6, 2);
            $table->text('description');
            $table->enum('active', ['1', '0'])->default('1');
            $table->smallInteger('max_option');
            $table->enum('is_extra', ['1', '0'])->default('1');
            $table->enum('is_optional', ['1', '0'])->default('0');
            $table->integer('user_entry');
            $table->integer('user_update');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
