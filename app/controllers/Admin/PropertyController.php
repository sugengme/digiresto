<?php  namespace Admin;

use Illuminate\Support\Facades\Input;
use Petruk\Properties\Repositories\PropertyRepositoryInterface;
use View;

class PropertyController extends \BaseController {

    protected $repo;

    public function __construct(PropertyRepositoryInterface $repository)
    {
        $this->repo = $repository;
    }

    public function store()
    {
        $this->repo->create(Input::all());
    }
}