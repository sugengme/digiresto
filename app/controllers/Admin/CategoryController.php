<?php  namespace Admin;

use Illuminate\Support\Facades\Input;
use Petruk\Categories\Repositories\CategoryRepositoryInterface;
use View;

class CategoryController extends \BaseController{
    protected $repo;

    public function __construct(CategoryRepositoryInterface $category)
    {
        $this->repo = $category;
    }

    public function store()
    {
        $this->repo->create(Input::all());
    }
}