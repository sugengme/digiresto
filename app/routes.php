<?php

Route::get('/', function()
{
	return View::make('admin.layout.default');
});

Route::group(['namespace' => 'Admin'], function()
{
    Route::post('property/add', [
        'as' => 'property.store',
        'uses'=>'PropertyController@store'
    ]);
});
