<?php  namespace Petruk\Provider;


use Illuminate\Support\ServiceProvider;
use Petruk\Categories\Category;
use Petruk\Categories\Repositories\CategoryRepository;
use Petruk\Properties\Property;
use Petruk\Properties\Repositories\PropertyRepository;

class PetrukServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->bootProperty();
        $this->bootCatergory();
    }

    protected function bootProperty()
    {
        $this->app->bind('Petruk\Properties\Repositories\PropertyRepositoryInterface', function () {
            return new PropertyRepository(new Property);
        });
    }

    protected function bootCatergory()
    {
        $this->app->bind('Petruk\Properties\Repositories\CategoryRepositoryInterface', function () {
            return new CategoryRepository(new Category);
        });
    }
}