<?php  namespace Petruk\Properties; 

use Illuminate\Database\Eloquent\Model;

class Property extends Model {
    protected $table = 'property';

    protected $fillable = [
        'name',
        'address',
        'phone1',
        'phone2',
        'email',
        'website'
    ];
}