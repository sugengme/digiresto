<?php  namespace Petruk\Properties\Repositories; 

use Petruk\Core\RepositoryTrait;
use Petruk\Properties\Property;

class PropertyRepository implements PropertyRepositoryInterface {
    use RepositoryTrait;

    /**
     * @var Property
     */
    private $model;

    public function __construct(Property $property)
    {
        $this->model = $property;
    }
} 