<?php  namespace Petruk\Items; 

use Illuminate\Database\Eloquent\Model;

class Item extends Model {
    protected $table = 'items';
    
    public function extras()
    {
        return $this->belongsToMany('\Petruk\Items\Item', 'item_extras', 'item_id', 'extra_id');
    }

    public function tags()
    {
        return $this->belongsToMany('\Petruk\Tags\Tag', 'item_tag');
    }

    public function images()
    {
        return $this->hasMany('\Petruk\Images\Image');
    }
}