<?php namespace Petruk\Core;

trait RepositoryTrait {
    public function all($columns = array('*'))
    {
        return $this->model->all($columns);
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function find($id, $columns = array('*'))
    {
        return $this->model->find($id, $columns);
    }

    public function destroy($ids)
    {
        return $this->model->destroy($ids);
    }
} 