<?php  namespace Petruk\Categories\Repositories;

use Petruk\Core\RepositoryTrait;

class CategoryRepository implements CategoryRepositoryInterface {
    use RepositoryTrait;
} 